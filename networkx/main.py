from models import User, Course
import networkx as nx

users = []
courses = []


def create_users():
    for username in ["Alice", "Bob", "Charlie"]:
        users.append(User(name=username))
    return users


def create_courses():
    for coursename in ["PXT-101", "Innovation", "Vue for dummies"]:
        courses.append(Course(name=coursename))
    return courses


def add_nodes(graph):
    graph.add_nodes_from(users)
    graph.add_nodes_from(courses)


def add_edges(graph):
    ids = [hash(user) for user in users]
    print(ids)
    relationship = (users[0], users[1])
    graph.add_edge(*relationship)
    graph.add_edge(users[1], users[2])
    graph.edges[users[0], users[1]]["weight"] = 1234
    graph.edges[users[0], users[1]]["friends"] = True


def print_graph(graph):
    print(graph)
    print(f"Nodes: {list(graph.nodes)}")
    print(f"Edges: {list(graph.edges)}")

    print(f"Edge relationship: {graph.edges[users[0], users[1]]}")


def print_node(graph, index):
    print(graph[index])


def query(graph):
    print(graph.adj)


def main():
    create_users()
    create_courses()

    graph = nx.DiGraph()
    add_nodes(graph)
    add_edges(graph)

    print_graph(graph)

    query(graph)


if __name__ == "__main__":
    main()
