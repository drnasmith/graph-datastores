graph [
  directed 1
  node [
    id 0
    label "1362900706209703906"
    uid "c58a0447-742e-45aa-8699-dcbd084ffa5d"
    name "Alice"
    tags [
      role "student"
    ]
  ]
  node [
    id 1
    label "2174882080071869024"
    uid "2dff2738-6234-44ee-ae35-81f78169bae0"
    name "Bob"
    tags [
      role "student"
    ]
  ]
  node [
    id 2
    label "508495875039234086"
    uid "f60feda5-c681-4472-968f-1d1fad6ce454"
    name "Charlie"
    tags [
      role "student"
    ]
  ]
  node [
    id 3
    label "1137557387190573879"
    uid "f16a34d7-9829-45a5-a477-c3ade5d36dce"
    name "PXT-101"
    tags [
    ]
  ]
  node [
    id 4
    label "1781964600091209512"
    uid "6c13a795-2f44-4da2-981d-92b3549f81f9"
    name "Innovation"
    tags [
    ]
  ]
  node [
    id 5
    label "1853046779983146029"
    uid "554781ab-b268-410a-af7b-4adcedd1a3c3"
    name "Vue for dummies"
    tags [
    ]
  ]
  edge [
    source 0
    target 3
    completed 1
  ]
]
