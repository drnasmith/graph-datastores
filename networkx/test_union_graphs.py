from models import User, Course
import networkx as nx

users = []
courses = []


def create_user_graph():
    g = nx.Graph()
    g.add_nodes_from(users)
    return g


def create_course_graph():
    g = nx.Graph()
    g.add_nodes_from(courses)
    return g


def create_users():
    for username in ["Alice", "Bob", "Charlie"]:
        users.append(User(name=username))
    return users


def create_courses():
    for coursename in ["PXT-101", "Innovation", "Vue for dummies"]:
        courses.append(Course(name=coursename))
    return courses


def build_graph():
    create_users()
    create_courses()

    ug = create_user_graph()
    cg = create_course_graph()

    total_graph = nx.union(ug, cg)

    return total_graph


def print_graph(graph):
    print(graph)
    print(f"Nodes: {list(graph.nodes)}")
    print(f"Edges: {list(graph.edges)}")


def main():
    graph = build_graph()
    print_graph(graph)


if __name__ == "__main__":
    main()
