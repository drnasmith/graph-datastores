import networkx as nx
from models import User, Course
import uuid
import json
from dsl import Query

users = []
courses = []


def reset():
    global users, courses
    users = []
    courses = []


class GraphDatastore:
    def __init__(self):
        self.graph = nx.DiGraph()

    def read(self):
        raise NotImplementedError

    def write(self):
        raise NotImplementedError

    def add_nodes(self):
        self.graph.add_nodes_from([(hash(user), user.dict()) for user in users])
        self.graph.add_nodes_from([(hash(course), course.dict()) for course in courses])

        self.graph.add_edge(hash(users[0]), hash(courses[0]), completed=True)

    def create_users(self):
        user_tags = {"role": "student"}
        for username in ["Alice", "Bob", "Charlie"]:
            users.append(User(name=username, tags=user_tags))
        return users

    def create_courses(self):
        for coursename in ["PXT-101", "Innovation", "Vue for dummies"]:
            courses.append(Course(name=coursename))
        return courses

    def print(self):
        print(self.graph)

    def dump(self, filename="graph.json"):
        db = nx.to_dict_of_dicts(self.graph)
        with open(filename, "w") as f:
            json.dump(db, f)


class PickleGraphDatastore(GraphDatastore):
    def read(self, filepath):
        self.graph = nx.read_gpickle(filepath)

    def write(self, filepath):
        nx.write_gpickle(self.graph, filepath)


class GmlDatastore(GraphDatastore):
    def read(self, filepath):
        self.graph = nx.read_gml(filepath)

    def write(self, filepath):
        nx.write_gml(self.graph, filepath, self._stringizer)

    def _stringizer(self, obj):
        if isinstance(obj, uuid.UUID):
            return str(obj)
        else:
            return obj


class GraphmlDatastore(GraphDatastore):
    """
    GraphML does not support dict attributes or cope well with non standard data types
    """

    def read(self, filepath):
        self.graph = nx.read_graphml(filepath)

    def write(self, filepath):
        nx.write_graphml(self.graph, filepath, named_key_ids=True)

    def _stringizer(self, obj):
        if isinstance(obj, uuid.UUID):
            return str(obj)
        else:
            return obj

    # Override this so we convert uuid to json
    def add_nodes(self):
        self.graph.add_nodes_from(
            [(hash(user), json.loads(user.json())) for user in users]
        )
        self.graph.add_nodes_from(
            [(hash(course), json.loads(course.json())) for course in courses]
        )

        self.graph.add_edge(hash(users[0]), hash(courses[0]))


class ResultQuery(Query):
    def __init__(self, graph):
        self._graph = graph
        self.recordset = []

    def user(self, name):
        for node in self._graph.nodes:
            if self._graph.nodes[node]["name"] == name:
                self.recordset.append(node)
        return self

    def completed(self, course):
        course_node = self.get_course_node(course)
        print(f"User nodes = {self.recordset}")
        print(f"Course node = {course_node}")

        print(f"completed edges = {self._graph.edges.data('completed')}")
        completed = self._graph.edges.data("completed")
        for result in completed:
            n1, n2, comp = result
            if n1 in self.recordset and n2 == course_node:
                print(f"Found matching nodes - completed = {comp}")

    def get_course_node(self, course):
        course_node = None
        for node in self._graph.nodes:
            if self._graph.nodes[node]["name"] == course:
                course_node = node
        return course_node


def export_graph():
    g = PickleGraphDatastore()
    g.create_courses()
    g.create_users()
    g.add_nodes()
    g.write("graph.pickle")


def import_graph():
    g = PickleGraphDatastore()
    g.read("graph.pickle")
    g.print()


def export_gml():
    g = GmlDatastore()
    g.create_courses()
    g.create_users()
    g.add_nodes()
    g.write("graph.gml")
    print(f"Label  = {hash(users[0])}")


def import_gml():
    g = GmlDatastore()
    g.read("graph.gml")
    g.print()
    label = hash(users[0])
    print(g.graph.nodes.data())
    print(g.graph.nodes[str(label)])

    q = ResultQuery(g.graph)
    q.user(name="Alice").completed(course="PXT-101")


def test_export_graphml():
    g = GraphmlDatastore()
    g.create_courses()
    g.create_users()
    g.add_nodes()
    g.write("graph.xml")


def test_import_graphml():
    g = GmlDatastore()
    g.read("graph.xml")
    g.print()
    print(g.graph.nodes.data())


def main():
    export_graph()
    import_graph()

    reset()

    export_gml()
    import_gml()


if __name__ == "__main__":
    main()
