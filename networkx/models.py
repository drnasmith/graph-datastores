import uuid
from pydantic import BaseModel, Field


class HashableModel(BaseModel):
    uid: uuid.UUID = Field(default_factory=uuid.uuid4)

    def __hash__(self):
        return hash(self.uid)


class Course(HashableModel):
    name: str
    tags: dict = {}


class User(HashableModel):
    name: str
    tags: dict = {}


class UserNoHash(BaseModel):
    uid: uuid.UUID = Field(default_factory=uuid.uuid4)
    name: str


if __name__ == "__main__":
    user = User(name="Alice")

    print(hash(user))

    no_hash_user = UserNoHash(name="Alice")

    print(hash(no_hash_user))
