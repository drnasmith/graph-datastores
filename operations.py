from typing import List


class Query:
    def __init__(self, datastore):
        self._datastore = datastore
        self._results = None
        self._offset = None
        self._limit = None

    def node(self, id: int, as_type=None):
        self._results[id] = self._datastore.get_node(id, as_type)
        return self

    def nodes(self, ids: List[int], as_type=None):
        self._results = []
        for id in ids:
            self._results.append(self._datastore.get_node(id, as_type))
        return self

    def label(self, label):
        self._results = self._datastore.get_nodes(label)
        return self

    def filter(self, key, value):
        try:
            self._results = self._filter_dict_nodes(self._results, key, value)
        except TypeError:
            self._results = self._filter_list_nodes(self._results, key, value)
        return self

    def all(self):
        try:
            self._results = [{key: value} for key, value in self._results.items()]
        except ValueError:
            print("Results are not a dict")

        if self._offset and self._limit:
            return self._results[self._offset : self._offset + self._limit]
        else:
            return self._results

    def limit(self, limit: int):
        if limit >= 0:
            self._limit = limit
        return self

    def offset(self, offset: int):
        if offset >= 0:
            self._offset = offset
        return self

    def first(self):
        if isinstance(list, self._results):
            return self._results[0]
        else:
            first_key = next(iter(self._results))
            return {first_key: self._results[first_key]}

    def out(self, label):
        results = {}
        for node in self._results:
            try:
                edges = self._datastore.edges()[node]
            except KeyError:
                # Not matching edges for the node, carry on...
                continue
            nodes = self._filter_dict_edges(edges, label)
            if nodes:
                results[node] = self._results[node]
                results[node]["out"] = nodes
        self._results = results
        return self

    def incoming(self, label=None):
        # Slow reverse traversal...
        results = {}
        edges = self._datastore.edges()

        for edge, relationship in edges.items():
            print(relationship)
            # relationship is the list of nodes connected to the
            for entry in relationship:
                node = entry.get("node")
                props = entry.get("properties")
                if node in self._results.keys():
                    if label is None or label == entry.get("label"):
                        results.setdefault(node, {})
                        results[node][edge] = {"label": label}
                        if props:
                            results[node][edge]["properties"] = props
        print(results)
        for node, edges in results.items():
            if node in self._results:
                for edge in edges.keys():
                    self._results[node].setdefault("incoming", {})
                    self._results[node]["incoming"] = edges

        nodes_with_no_edges = list(set(self._results.keys()) - set(results.keys()))

        for node in nodes_with_no_edges:
            try:
                del self._results[node]
            except Exception:
                self._results.remove(node)
        return self

    def _filter_list_nodes(self, nodes, attribute_key, attribute_value):
        return [
            node
            for node in nodes
            if node.properties().get(attribute_key) == attribute_value
        ]

    def _filter_dict_nodes(self, nodes, attribute_key, attribute_value):
        filtered = {
            key: value
            for key, value in nodes.items()
            if value.properties().get(attribute_key) == attribute_value
        }
        return filtered

    def _filter_list_edges(self, edges, label):
        return [edge for edge in edges if edge.get("label") == label]

    def _filter_dict_edges(self, edges, label):
        filtered = {
            entry.get("node"): entry for entry in edges if entry.get("label") == label
        }
        return filtered

    def filter_by_label(self, label):
        self._results = {
            key: value for key, value in self._results.items() if value.label == label
        }
        return self

    def __str__(self):
        return f"<Query Resultset> {self._results}"


class Operation:
    def __init__(self, user, datastore):
        self._user = user
        self._datastore = datastore
        self._snapshot = None
        self._results = {}

    def use_snapshot(self, snapshot: int):
        self._snapshot = self._datastore.get_node(snapshot)
        return self

    def use_branch(self, branch: str):
        # Find the node with this branch name
        nodes = self._datastore.get_nodes(label="Branch")
        branch_node = None
        for (id, value) in nodes.items():
            if value.name == branch:
                branch_node = id
        if branch_node:
            edges = self._datastore.get_edges(branch_node, label="hasSnapshot")
            self._snapshot = self._datastore.get_node(edges[0].endNodeId)
        return self

    def all(self):
        return self._results

    def first(self):
        first_key = next(iter(self._results))
        return {first_key: self._results[first_key]}


class CourseOperation(Operation):
    def __init__(self, user, datastore):
        super().__init__(user, datastore)
        self._result = None
        self._id = None

    def get_course(self, course_id):
        # Is course on snapshot?
        self._course_access_allowed(course_id)
        self._result = self._get_node(course_id, type="Course")
        return self

    def get_courses(self):
        return self

    def add_course(self, course):
        return self

    def update_course(self, id, course):
        return self

    def remove_course(self, id):
        return self

    def result(self):
        return self._result

    def id(self):
        return self._id

    def _course_access(self, course_id):
        if self._snapshot:
            edges = self._datastore.get_edges(
                start_node_id=self._snapshot.id,
                end_node_id=course_id,
                label="hasCourse",
            )
            if not edges:
                raise Exception("Error permission denied")


# operations().use_snapshot("main").add_course()
# op = operations().use_snapshot(123)
# op.get_courses()
# op.get_users()
# op.rollback()
