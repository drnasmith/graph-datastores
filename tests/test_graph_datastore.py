from graph_datastore import Datastore, UserQuery, CourseQuery


def test_load_store(filename):
    d = Datastore(filename)
    print(d.db)


def test_get_users(filename):
    d = Datastore(filename)
    # print(d.nodes())

    q = UserQuery(d)
    print(f"All Users {q.users().all()}")


def test_get_courses(filename):
    d = Datastore(filename)
    q = CourseQuery(d)

    print(f"All Courses {q.courses().all()}")


def test_get_users_completed(filename):
    d = Datastore(filename)

    q = UserQuery(d)
    results = q.users().filter("color", "red").out("completed").all()
    print(f"All Users completed a course: {results}")


def test_get_users_from_course(filename):
    d = Datastore(filename)
    q = CourseQuery(d)

    nodes = (
        q.courses()
        .filter("name", "https://pxt.plexsys.com/pxt-101")
        .incoming("completed")
        .all()
    )

    print(nodes)


if __name__ == "__main__":
    test_get_users("db.json")
    test_get_courses("db.json")
    test_get_users_completed("db.json")
    test_get_users_from_course("db.json")
