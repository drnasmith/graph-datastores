from typing import Optional, List
from pydantic import create_model, BaseModel, Field, validator


class Activity(BaseModel):
    activityId: str


class Course(BaseModel):
    id: int = 0
    title: str
    description: str = ""
    activities: Optional[List[Activity]] = []


def unique_id():
    import uuid

    return uuid.uuid4().int


class Node(BaseModel):
    id: int = Field(default_factory=unique_id)
    label: str
    properties: dict = {}
    # Will be validated last.
    # Important because that means we can generate a hash of properties on init
    # If properties are added later, generate a new property_id by hashing properties
    property_id: int = None

    @validator("property_id", always=True)
    def set_property_id(cls, v, values, **kwargs):
        """Set the Property Id based on hash or passed value"""
        return v or cls.generate_property_hash(values.get("properties"))

    @staticmethod
    def generate_property_hash(props):
        """
        Dictionaries are not hashable.
        So we convert to a model, then use pydantic to serialize into a json
        string value (which is hashable).
        """
        if props:
            NodePropertyModel = create_model("NodePropertyModel", **props)
            return hash(NodePropertyModel().json())
        else:
            return None


def test_create_simple_model():
    data = {"name": "Course 1", "description": "My Course"}

    PropertyModel = create_model("PropertyModel", **data)

    model = PropertyModel()

    assert model.name == data["name"]
    assert model.description == data["description"]


def test_create_nested_model():
    data = {
        "name": "Course 1",
        "description": "My Course",
        "user": {"name": "Test", "id": 1234},
    }

    PropertyModel = create_model("PropertyModel", **data)

    model = PropertyModel()

    assert model.name == data["name"]
    assert model.description == data["description"]
    # Note nested model is not dynamic - saved as a dict
    assert model.user["name"] == "Test"


def test_convert_generic_model():
    data = {
        "id": 123,
        "title": "Course 1",
        "description": "My Course",
        "activities": [{"activityId": "http://activities/1"}],
    }
    PropertyModel = create_model("PropertyModel", **data)

    model = PropertyModel()
    assert model.id == 123
    assert len(model.activities) == 1
    # Generic Model converts to a dict
    assert model.activities[0]["activityId"] == "http://activities/1"

    properties = model.dict()
    course = Course(**properties)
    assert course.id == 123
    assert len(course.activities) == 1
    # Converted Model converts to a dict?
    assert course.activities[0].activityId == "http://activities/1"


def test_node_model():
    model = Course(id=123, title="My Course")
    properties = model.dict(exclude={"id"})
    node = Node(id=model.id, label=model.__class__.__name__, properties=properties)
    print(node.property_id)
    assert node.property_id == hash(model.json(exclude={"id"}))

    # Could also assign property id on construction
    # Would need to fetch properties from the db
    node = Node(**{"id": 123, "label": "mock_model", "property_id": 1234})
    assert node.property_id == 1234

    data = {"first_name": "Bob", "last_name": "Smith"}
    node = Node(**{"id": 123, "label": "mock_model"})
    node.properties = data
    node.property_id = node.generate_property_hash(data)
    MockModel = create_model("MockModel", **data)
    assert node.property_id == hash(MockModel().json())


def test_retrieve_model():
    db = {
        "nodes": {"1": {"label": "Course", "property_id": 1}},
        "properties": {
            "1": {
                "title": "My Course",
                "description": "Test Course",
                "activities": [{"activityId": "http://activities/1"}],
            }
        },
    }
    # Get Model from node
    node_id = 1
    node = Node(**(db["nodes"][str(node_id)] | {"id": node_id}))
    node.properties = db["properties"][str(node.property_id)]
    data = node.dict(exclude={"label", "property_id"}) | node.properties
    course = Course(**data)
    assert course.id == node_id
    assert course.title == "My Course"
    assert course.description == "Test Course"
