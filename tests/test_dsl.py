import pytest
from memory_datastore import Datastore
from entities import Activity, Branch, Course, Snapshot
from db_dsl import Query, Operation


@pytest.fixture
def courses():
    return [
        Course(title=f"Course {i}", description=f"test-course-{i}") for i in range(3)
    ]


@pytest.fixture
def activities():
    return [
        Activity(title=f"Activity {i}", activityId=f"http://activities/test-course-{i}")
        for i in range(5)
    ]


@pytest.fixture
def init_datastore(courses, activities):
    datastore = Datastore()
    for course in courses:
        datastore.add_node(course)

    for activity in activities:
        datastore.add_node(activity)

    parent = None
    for i in range(3):
        node = Snapshot()
        nodeId = datastore.add_node(node)
        if parent:
            datastore.add_edge(nodeId, end_node_id=parent, label="parent")
        parent = nodeId

    yield datastore

    del datastore


def test_init_datastore(init_datastore):
    datastore = init_datastore
    datastore.dump("init-db.json")


def test_filter_nodes_by_label(init_datastore):
    datastore = init_datastore
    query = Query(datastore)
    results = query.label("Course").all()
    assert len(results) == 3

    query = Query(datastore)
    results = query.label("Activity").all()
    assert len(results) == 5


def test_snapshot_operations(init_datastore):
    datastore = init_datastore
    main_branch = Branch(name="main")
    main_branch.id = datastore.add_node(main_branch)

    public_branch = Branch(name="public")
    public_branch.id = datastore.add_node(public_branch)

    snapshots = datastore.get_nodes("Snapshot")
    iterator = iter(snapshots)
    first_snapshot = next(iterator)
    second_snapshot = next(iterator)

    assert first_snapshot != second_snapshot

    datastore.add_edge(main_branch.id, first_snapshot, label="hasSnapshot")
    datastore.add_edge(public_branch.id, second_snapshot, label="hasSnapshot")

    datastore.dump("test-snapshot.json")

    operation = Operation(datastore)
    operation.use_snapshot(first_snapshot)

    assert operation._snapshot.id == first_snapshot

    operation = Operation(datastore)
    operation.use_branch("public")
    assert operation._snapshot.id == second_snapshot
