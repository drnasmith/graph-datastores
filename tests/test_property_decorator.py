from pydantic import BaseModel


class Node(BaseModel):
    id: int = 0
    _name: str = ""

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value


class Circle:
    def __init__(self, radius):
        self._radius = radius

    @property
    def radius(self):
        """The radius property."""
        print("Get radius")
        return self._radius

    @radius.setter
    def radius(self, value):
        print("Set radius")
        self._radius = value

    @radius.deleter
    def radius(self):
        print("Delete radius")
        del self._radius


if __name__ == "__main__":
    node = Node()
    name = node.name
    node.name = "hello"
    name = node.name
    print(name)

    circle = Circle(32)
    circle.radius = 200
    rad = circle.radius
