from memory_datastore import Datastore
from db_models import Node
from entities import Activity, Course, Snapshot


def test_read_pure_nodes():
    datastore = Datastore()

    node1 = Node()

    datastore.add_node(node1)
    datastore.dump("nodes.json")


def test_read_write_nodes():
    datastore = Datastore()

    course1 = Course(title="Course 1", description="Test course")

    id = datastore.add_node(course1)

    properties = datastore.get_node(id)
    assert properties.title == "Course 1"


def test_update_node():
    datastore = Datastore()
    course1 = Course(title="Course 1", description="Test course")
    id = datastore.add_node(course1)

    properties = datastore.get_node(id)
    assert properties.title == "Course 1"

    # Change properties of course and save
    course1.id = id
    course1.title = "Course 1 Ext"
    course1.description = "Advanced course"
    datastore.save_node(course1)

    properties = datastore.get_node(course1.id, type="dict")
    assert properties["title"] == "Course 1 Ext"
    assert properties["id"] == course1.id


def test_get_course():
    datastore = Datastore()
    course1 = Course(title="Course 1", description="Test course")
    id = datastore.add_node(course1)

    course = datastore.get_node(id, type="Course")
    assert course.title == "Course 1"


def test_get_courses():
    datastore = Datastore()
    for i in range(3):
        course = Course(title=f"Course {i}", description=f"Test-course-{i}")
        _ = datastore.add_node(course)

    courses = datastore.get_nodes("Course")
    assert len(courses) == 3


def test_get_edges():
    datastore = Datastore()
    snapshot = Snapshot()
    course1 = Course(title="Course 1", description="Test course")

    id = datastore.add_node(course1)
    course1.id = id

    id = datastore.add_node(snapshot)
    snapshot.id = id

    datastore.add_edge(
        start_node_id=snapshot.id, end_node_id=course1.id, label="hasCourse"
    )

    edges = datastore.get_edges(snapshot.id)
    assert len(edges) == 1
    assert edges[0].startNodeId == snapshot.id
    assert edges[0].label == "hasCourse"


def test_dump_datastore():
    datastore = Datastore()
    snapshot = Snapshot()
    course1 = Course(title="Course 1", description="Test course")
    activity1 = Activity(title="Activity 1", activityId="Test activity")

    for node in [snapshot, course1, activity1]:
        id = datastore.add_node(node)
        node.id = id

    datastore.add_edge(
        start_node_id=snapshot.id, end_node_id=course1.id, label="hasCourse"
    )
    datastore.add_edge(
        start_node_id=course1.id, end_node_id=activity1.id, label="hasActivity"
    )

    datastore.dump("output.json")


def test_filter_nodes():
    datastore = Datastore()
    for i in range(3):
        course = Course(title=f"Course {i}", description=f"Test course {i}")
        datastore.add_node(course)

    for i in range(3):
        activity = Activity(title=f"Activity {i}", activityId=f"http://activities/{i}")
        datastore.add_node(activity)

    nodes = datastore.filter_nodes("Course", {"title": "Course 1"})
    assert len(nodes) == 1
    first_match = next(iter(nodes))
    assert nodes[first_match].title == "Course 1"
    assert nodes[first_match].description == "Test course 1"

    nodes = datastore.filter_nodes(
        "Course", {"title": "Course 1", "description": "No match"}
    )
    assert len(nodes) == 0
