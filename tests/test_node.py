import pytest
from pydantic import ValidationError
from db_models import Node
from entities import Activity, Course, User


def test_nodes():
    node0 = Node()
    node1 = Node()
    node2 = Node(label="Course")

    assert node0.label == "Node"
    assert node1.label == "Node"
    assert node2.label == "Course"
    assert node0.id != node1.id
    assert node0.id is not node1.id


def test_serialize_models():
    course1 = Course(title="Course 1", description="test course")
    assert course1.dict().get("title") == "Course 1"
    assert course1.dict().get("description") == "test course"

    assert "id" not in course1.properties()


def test_hash_model():
    course1 = Course(title="Course 1", description="test course")
    # Hash of properties should equal the dictionary of values
    assert hash(course1) == hash(
        course1.json(include={key for key in course1.properties()})
    )


def test_hash_nodes():
    course1 = Course(title="Course 1", description="test course")
    course2 = Course(title="Course 2", description="test course")
    course1_clone = Course(title="Course 1", description="test course")

    assert hash(course1) == hash(course1_clone)
    assert hash(course1) != hash(course2)


def test_models():
    _ = Course(title="Course 1", description="Description 1")
    _ = Activity(title="Activity 1", activityId="http://activities/1")
    _ = User(name="User 1")

    with pytest.raises(ValidationError):
        _ = Course()
