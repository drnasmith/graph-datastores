from pydantic import BaseModel


class Entity(BaseModel):
    id: int = None

    def __hash__(self):
        data = self.json(exclude={"id"})
        return hash(data)

    def properties(self):
        return self.dict(exclude={"id"})


class Course(Entity):
    title: str
    description: str
    activities: list = []


class User(Entity):
    name: str


class Activity(Entity):
    title: str
    activityId: str


class Snapshot(Entity):
    ...


class Branch(Entity):
    name: str
