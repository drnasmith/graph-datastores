class Query:
    def __init__(self):
        self._results = None
        self._offset = None
        self._limit = None

    def filter(self, key, value):
        # Filter result set by user
        self._results = [result for result in self._results if result.get(key) == value]
        return self

    def all(self):
        if self._offset and self._limit:
            return self._results[self._offset : self._offset + self._limit]
        else:
            return self._results

    def limit(self, limit: int):
        if limit >= 0:
            self._limit = limit
        return self

    def offset(self, offset: int):
        if offset >= 0:
            self._offset = offset
        return self

    def first(self):
        return self._results[0]


class Course(Query):
    def get(self, course):
        # Get all results for course
        self._results = [
            {"user": "bob", "score": 23},
            {"user": "bob", "score": 25},
            {"user": "alice", "score": 44},
            {"user": "jim", "score": 32},
            {"user": "fred", "score": 33},
            {"user": "boaty", "score": 55},
        ]
        return self


if __name__ == "__main__":
    q = Course()
    results = q.get("course1").filter("user", "bob").all()
    print(f"All Bob's results {results}")

    results = q.get("course1").filter("user", "bob").first()
    print(f"Bob's first result {results}")

    results = q.get("course1").all()
    print(f"All results {results}")

    results = q.get("course1").offset(2).limit(2).all()
    print(f"Offset, limit results {results}")
