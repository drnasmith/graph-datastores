import json
from db_models import Node, Edge, EdgesCollection
from entities import Activity, Course, Entity, Snapshot, Branch


class ModelFactory:
    _registry = {}

    def create(self, data, type="dict"):
        if type == "dict" or type == "json":
            return data
        else:
            return self._registry[type](**data)

    def register(self, name, handler):
        self._registry[name] = handler

    def unregister(self, name):
        del self._registry[name]


class Datastore:
    def __init__(self):
        self._datastore = {"nodes": {}, "edges": {}, "objects": {}}
        self._factory = ModelFactory()
        self._factory.register("Node", Node)
        self._factory.register("Activity", Activity)
        self._factory.register("Course", Course)
        self._factory.register("Snapshot", Snapshot)
        self._factory.register("Branch", Branch)
        self._factory.register("Edge", Edge)
        self._factory.register("EdgesCollection", EdgesCollection)

    def add_node(self, object: Entity):
        node = Node(label=object.__class__.__name__, objectId=hash(object))
        self._insert_node(node, object)
        return node.id

    def save_node(self, object: Entity):
        return self._update_node(object)

    def get_node(self, nodeId: int, type=None) -> Entity:
        node_properties = self._datastore["nodes"][nodeId]
        object_properties = self._get_object(node_properties.get("objectId")) or {}

        full_data = object_properties | {"id": nodeId}
        return self._factory.create(full_data, type or node_properties["label"])

    def get_nodes(self, label: str) -> Entity:
        filter_function = create_label_filter(label=label)
        results = {
            key: value
            for (key, value) in self._datastore["nodes"].items()
            if filter_function(value)
        }
        return {key: self.get_node(key) for key in results}

    def filter_nodes(self, label: str, properties: dict = {}) -> Entity:
        filter_function = create_label_filter(label=label)
        results = {
            key: value
            for (key, value) in self._datastore["nodes"].items()
            if filter_function(value)
        }
        filter_nodes_function = create_property_filter(properties)
        results = {
            key: self.get_node(key)
            for (key, value) in results.items()
            if filter_nodes_function(self._get_object(value.get("objectId")))
        }
        return results

    def add_edge(self, start_node_id: int, end_node_id: int, label: str) -> int:
        edge = Edge(startNodeId=start_node_id, endNodeId=end_node_id, label=label)
        return self._insert_edge(edge)

    def save_edge(self, edge: Edge):
        return self._update_edge(edge)

    def get_edges(self, start_node_id: int, end_node_id: int = None, label: str = None):
        filter_function = create_edge_filter(
            start_id=start_node_id, end_id=end_node_id, label=label
        )
        results = {
            key: value
            for (key, value) in self._datastore["edges"].items()
            if filter_function(value)
        }
        return [
            self._factory.create(value | {"id": key}, type="Edge")
            for (key, value) in results.items()
        ]

    def _update_node(self, object):
        if object.id not in self._datastore["nodes"]:
            raise KeyError("Node with this id does not exist")

        node = Node(
            id=object.id, label=object.__class__.__name__, objectId=hash(object)
        )
        self._datastore["objects"][node.objectId] = object.properties()
        self._datastore["nodes"][node.id] = node.dict(exclude={"id"})

        return node.id

    def _insert_node(self, node: Node, object: Entity):
        if node.id in self._datastore["nodes"]:
            raise ValueError("Node with this id already exists")

        self._datastore["objects"][node.objectId] = object.properties()
        self._datastore["nodes"][node.id] = node.dict(exclude={"id"})

        return node.id

    def _update_edge(self, edge):
        if edge.id not in self._datastore["edges"]:
            raise KeyError(f"Edge with id {edge.id} does not exist")
        self._datastore["edges"][edge.id] = edge.dict(exclude={"id"})

    def _insert_edge(self, edge):
        # Store references in indexes...
        # startNodeId = edge.startNodeId (outgoing)
        # endNodeId = edge.endNodeId (incoming)
        if edge.id in self._datastore["edges"]:
            raise ValueError(f"Edge with id {edge.id} already exists")
        self._datastore["edges"][edge.id] = edge.dict(exclude={"id"})
        return edge.id

    def _get_object(self, objectId):
        return self._datastore["objects"][objectId]

    def dump(self, fileobj):
        json_data = json.dumps(self._datastore, indent=4)
        with open(fileobj, "w") as file:
            file.write(json_data)


def create_edge_filter(start_id=None, end_id=None, label=None):
    def filter_edge(edge):
        start_match = start_id == edge["startNodeId"] or start_id is None
        end_match = end_id == edge["endNodeId"] or end_id is None
        label_match = label == edge["label"] or label is None
        return all([start_match, end_match, label_match])

    return filter_edge


def create_label_filter(label=None):
    def filter_label(node):
        label_match = label == node["label"] or label is None
        return all([label_match])

    return filter_label


def create_property_filter(properties: dict = {}):
    def filter_node(node):
        match = True
        for key, value in properties.items():
            if key not in node:
                return False
            elif node[key] != value:
                return False
        return match

    return filter_node
