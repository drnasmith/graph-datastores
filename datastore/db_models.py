import uuid
from typing import List, Optional
from pydantic import BaseModel, Field, validator


def unique_id():
    return uuid.uuid4().int


class Edge(BaseModel):
    id: int = Field(default_factory=unique_id)
    startNodeId: int
    endNodeId: int
    label: str


class EdgesCollection(BaseModel):
    nodeId: int
    edges: List[Edge] = []

    def add_node(self, node, label):
        edge = Edge(startNodeId=self.nodeId, endNodeId=node.id, label=label)
        self.edges.append(edge)

    def remove_node(self, node, label):
        for edge in self.edges:
            if edge.startNodeId == node.id and edge.label == label:
                self.edges.remove(edge)
                return True
        return False


class Node(BaseModel):
    id: int = Field(default_factory=unique_id)
    label: Optional[str]
    objectId: int = None

    @validator("label", always=True)
    def set_label(cls, value):
        return value or cls.__name__

    def __hash__(self):
        data = self.json(exclude={"id"})
        return hash(data)

    def properties(self):
        return self.dict(exclude={"id"})
