import json
import re
import uuid
from typing import Any


uuid_pattern = re.compile(
    "[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}"
)


def generate_id() -> str:
    """
    Generate id should be moved to another service in future
    Used to create indexes as a proxy for database primary keys
    """
    return str(uuid.uuid4())


def json_handler(dct):
    """
    Convert UUID format strings into UUID class
    Needed to convert on disk string representations into UUID classes
    Extend to support other tricky formats as required (e.g. datetime/iso8601)
    """
    for key, value in dct.items():
        if type(value) == str and uuid_pattern.match(value):
            dct[key] = uuid.UUID(value)
    return dct


class CustomJSONSerializer(json.JSONEncoder):
    def default(self, value: Any) -> str:
        """
        Required because we store the "db" as a python dict and use json.dump to write out json
        UUIDs are not json serializable but we are writing uuids into our db
        May need to expand this to include other serialisation options
        """
        if isinstance(value, uuid.UUID):
            return str(value)

        return super(CustomJSONSerializer, self).default(value)


class Datastore:
    def __init__(self, filename):
        self.db = {}
        self.filename = filename
        try:
            self._load_data_from_file(filename)
        except (FileNotFoundError, NotADirectoryError):
            raise FileNotFoundError from None

    def dump_file(self, filename):
        """
        Method to help with testing - save contents to another file
        """
        with open(filename, "w") as file:
            json.dump(self.db, file, cls=CustomJSONSerializer)

    def _load_data_from_file(self, filename):
        if filename is None:
            raise FileNotFoundError
        try:
            with open(filename) as file:
                self.db = json.load(file, object_hook=json_handler)
        except FileNotFoundError:
            raise FileNotFoundError from None

    def _get_dict_from_path(self, path):
        try:
            path_parts = path.split(".")
            val = self.db
            for part in path_parts:
                val = val[part]
            return val
        except KeyError:
            raise KeyError(f"Could not find path {path}")

    def _write_data_to_file(self):
        with open(self.filename, "w") as file:
            json.dump(self.db, file, cls=CustomJSONSerializer)

    def nodes(self):
        return self._get_dict_from_path("nodes")

    def edges(self):
        return self._get_dict_from_path("edges")
