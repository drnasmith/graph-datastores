class Query:
    def __init__(self, datastore):
        self._datastore = datastore
        self._results = None
        self._offset = None
        self._limit = None

    def filter(self, key, value):
        try:
            self._results = self._filter_dict_nodes(self._results, key, value)
        except TypeError:
            self._results = self._filter_list_nodes(self._results, key, value)
        return self

    def all(self):
        try:
            self._results = [{key: value} for key, value in self._results.items()]
        except ValueError:
            print("Results are not a dict")

        if self._offset and self._limit:
            return self._results[self._offset : self._offset + self._limit]
        else:
            return self._results

    def limit(self, limit: int):
        if limit >= 0:
            self._limit = limit
        return self

    def offset(self, offset: int):
        if offset >= 0:
            self._offset = offset
        return self

    def first(self):
        if isinstance(list, self._results):
            return self._results[0]
        else:
            return self._results.values()[0]

    def out(self, label):
        results = {}
        for node in self._results:
            try:
                edges = self._datastore.edges()[node]
            except KeyError:
                # Not matching edges for the node, carry on...
                continue
            nodes = self._filter_dict_edges(edges, label)
            if nodes:
                results[node] = self._results[node]
                results[node]["out"] = nodes
        self._results = results
        return self

    def incoming(self, label=None):
        # Slow reverse traversal...
        results = {}
        edges = self._datastore.edges()

        for edge, relationship in edges.items():
            print(relationship)
            # relationship is the list of nodes connected to the
            for entry in relationship:
                node = entry.get("node")
                props = entry.get("properties")
                if node in self._results.keys():
                    if label is None or label == entry.get("label"):
                        results.setdefault(node, {})
                        results[node][edge] = {"label": label}
                        if props:
                            results[node][edge]["properties"] = props
        print(results)
        for node, edges in results.items():
            if node in self._results:
                for edge in edges.keys():
                    self._results[node].setdefault("incoming", {})
                    self._results[node]["incoming"] = edges

        nodes_with_no_edges = list(set(self._results.keys()) - set(results.keys()))

        for node in nodes_with_no_edges:
            try:
                del self._results[node]
            except:
                self._results.remove(node)
        return self

    def _filter_list_nodes(self, nodes, attribute_key, attribute_value):
        return [
            node
            for node in nodes
            if node["properties"].get(attribute_key) == attribute_value
        ]

    def _filter_dict_nodes(self, nodes, attribute_key, attribute_value):
        filtered = {
            key: value
            for key, value in nodes.items()
            if value["properties"].get(attribute_key) == attribute_value
        }
        return filtered

    def _filter_list_edges(self, edges, label):
        return [edge for edge in edges if edge.get("label") == label]

    def _filter_dict_edges(self, edges, label):
        filtered = {
            entry.get("node"): entry for entry in edges if entry.get("label") == label
        }
        return filtered

    def filter_by_label(self, label):
        self._results = {
            key: value
            for key, value in self._results.items()
            if value["label"] == label
        }
        return self

    def __str__(self):
        return f"<Query Resultset> {self._results}"


class UserQuery(Query):
    def users(self):
        self._results = self._datastore.nodes()
        self.filter_by_label("user")
        return self


class CourseQuery(Query):
    def courses(self):
        self._results = self._datastore.nodes()
        self.filter_by_label("course")
        return self
